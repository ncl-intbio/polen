package org.ico2s.polen;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import java.util.List;

import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.admin.HalAdmin;
import com.microbasecloud.halogen.admin.HalAdminWsClient;
import com.microbasecloud.halogen.subscriber.Subscription;
import com.microbasecloud.util.GsonFactory;
import org.sbolstandard.core2.*;

public class Main {

  private static final String POLEN_BASE_URL = "https://polen.ico2s.org/demo/insecure";

  static UUID publisherId = UUID.nameUUIDFromBytes("test3".getBytes(StandardCharsets.UTF_8));
  static UUID subscriberId = UUID.nameUUIDFromBytes("subscriberTest3".getBytes(StandardCharsets.UTF_8));
  static UUID secretKey = UUID.nameUUIDFromBytes("secret3".getBytes(StandardCharsets.UTF_8));

  public static void main(String[] args) throws Exception {
    testRegister();
    testPublish();
    testConsume();
  }

  public static void testRegister() throws Exception {
    HalAuth halAdminAuth = new HalAuth(UUID.randomUUID(), UUID.randomUUID());
    HalAdmin admin = new HalAdminWsClient(GsonFactory.make(), POLEN_BASE_URL, halAdminAuth);

    Subscription subscription = new Subscription();
    subscription.setChannel("testChannel");
    subscription.setTopic("testTopic");

    admin.makeSubscriber(subscription, subscriberId, secretKey);
    admin.makePublisher(publisherId, secretKey);
  }

  public static void testPublish() throws Exception {
    PolenPublisher publisher = new PolenPublisher(POLEN_BASE_URL, publisherId, secretKey);

    publisher.publishFASTA(">test\nMVKTVVTVVTKAKKTASQUR", "testChannel", "testTopic");
  }

  public static void testConsume() throws Exception {
    PolenSubscriber subscriber = new PolenSubscriber(POLEN_BASE_URL, subscriberId, secretKey);

    SBOLAdapter subscription = subscriber.subscribeToSBOL("testChannel", "testTopic");
    List<SBOLDocument> sbol = subscription.consume(2000);

    for (SBOLDocument doc : sbol) {
      SBOLWriter.write(doc, System.out);
    }
  }
}

