package org.ico2s.polen;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.Subscription;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLValidationException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


/**
 * @author james
 * @author keith
 */
public class SBOLAdapter {
  private PolenSubscriber polenSubscriber;
  private Subscription subscription;

  public SBOLAdapter(PolenSubscriber polenSubscriber, Subscription subscription) {
    this.polenSubscriber = polenSubscriber;
    this.subscription = subscription;
  }

  public List<SBOLDocument> consume(int n) throws ConsumeException {
    HalSubscriber<String> subscriber = polenSubscriber.getSubscriber();

    try {
      return messagesToSbol(subscriber.consume(n, true));
    } catch (SBOLValidationException e) {
      throw new ConsumeException(e);
    } catch (SBOLConversionException e) {
      throw new ConsumeException(e);
    } catch (IOException e) {
      throw new ConsumeException(e);
    }
  }

  private List<SBOLDocument> messagesToSbol(List<Message<String>> messages)
      throws SBOLValidationException, SBOLConversionException, IOException {
    List<SBOLDocument> sbol = new ArrayList<SBOLDocument>();

    for (Message<String> message : messages) {
      ByteArrayInputStream stream = new ByteArrayInputStream(message.getContent().getBytes(StandardCharsets.UTF_8));

      SBOLReader.setURIPrefix("http://ico2s.github.io/polen/");
      sbol.add(SBOLReader.read(stream));
    }

    return sbol;
  }
}
