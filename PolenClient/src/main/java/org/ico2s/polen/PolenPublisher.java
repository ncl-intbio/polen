package org.ico2s.polen;

import com.google.gson.Gson;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.publisher.HalPublisherWsClient;
import com.microbasecloud.halogen.wrappers.BasicMessage;
import com.microbasecloud.util.GsonFactory;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * @author james
 * @author keith
 */
public class PolenPublisher {
  private HalPublisher<String> publisher;

  public PolenPublisher(HalPublisher<String> publisher) {
    this.publisher = publisher;
  }

  public PolenPublisher(String backendUrl, UUID publisherId, UUID secretKey) {
    this(new HalPublisherWsClient(GsonFactory.make(), backendUrl, publisherId, secretKey));
  }

  public void publishSBOL(SBOLDocument doc, String channel, String topic) throws PublishException {
    Message<String> msg = new BasicMessage<>();

    msg.getHeader().setType("sbol");
    msg.getHeader().setChannel(channel);
    msg.getHeader().setTopic(topic);

    try {
      msg.setContent(sbolToString(doc));
    } catch (UnsupportedEncodingException e) {
      throw new PublishException(e);
    } catch (SBOLConversionException e) {
      throw new PublishException(e);
    }

    publisher.publish(msg);
  }

  public void publishGenBank(String gbf, String channel, String topic) throws PublishException {
    Message<String> msg = new BasicMessage<>();

    msg.getHeader().setType("genbank");
    msg.getHeader().setChannel(channel);
    msg.getHeader().setTopic(topic);
    msg.setContent(gbf);

    publisher.publish(msg);

  }

  public void publishFASTA(String fasta, String channel, String topic) throws PublishException {
    Message<String> msg = new BasicMessage<>();

    msg.getHeader().setType("fasta");
    msg.getHeader().setChannel(channel);
    msg.getHeader().setTopic(topic);
    msg.setContent(fasta);

    publisher.publish(msg);
  }

  String sbolToString(SBOLDocument doc) throws UnsupportedEncodingException, SBOLConversionException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    SBOLWriter.write(doc, byteArrayOutputStream);
    return new String(byteArrayOutputStream.toByteArray(), StandardCharsets.UTF_8);
  }

  public void publishResource(String channel, String topic, Resource resource) {
    Message<String> msg = new BasicMessage<>();

    Gson gson = new Gson();

    msg.getHeader().setType("resource");
    msg.getHeader().setChannel(channel);
    msg.getHeader().setTopic(topic);
    msg.setContent(gson.toJson(resource));

    publisher.publish(msg);
  }

  public void publishResource(String channel, String topic, String resourceUri, String name, String description) {
    Resource resource = new Resource();
    resource.setResourceUri(resourceUri);
    resource.setName(name);
    resource.setDescription(description);

    publishResource(channel, topic, resource);
  }
}
