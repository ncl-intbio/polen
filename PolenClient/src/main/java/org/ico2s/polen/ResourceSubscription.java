package org.ico2s.polen;

import com.google.gson.Gson;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.Subscription;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 26/05/17.
 *
 * @author james
 * @author Keith
 */
public class ResourceSubscription {
  private PolenSubscriber polenSubscriber;
  private Subscription subscription;

  public ResourceSubscription(PolenSubscriber polenSubscriber, Subscription subscription) {
    this.polenSubscriber = polenSubscriber;
    this.subscription = subscription;
  }


  public List<Resource> consume(int n) throws ConsumeException {
    HalSubscriber<String> subscriber = polenSubscriber.getSubscriber();

    return messagesToResources(subscriber.consume(n));

  }

  List<Resource> messagesToResources(List<Message<String>> messages) {
    List<Resource> resources = new ArrayList<>();
    Gson gson = new Gson();
    for (Message<String> message : messages) {
      resources.add(gson.fromJson(message.getContent(), Resource.class));
    }
    return resources;
  }
}
