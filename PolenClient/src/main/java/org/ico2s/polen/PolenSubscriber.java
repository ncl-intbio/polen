package org.ico2s.polen;

import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.HalSubscriberWsClient;
import com.microbasecloud.halogen.subscriber.Subscription;
import com.microbasecloud.util.GsonFactory;

import java.util.UUID;

public class PolenSubscriber {

  // TODO: we need a Subscription here
  // TODO: move channek / topic / type to constructor
  // TODO: remove unnecessary methods and remove subscription from the SBOL Adapter

  private HalSubscriber<String> subscriber;

  public PolenSubscriber(HalSubscriber<String> subscriber) {
    this.subscriber = subscriber;
  }

  public PolenSubscriber(String backendUrl, UUID subscriberId, UUID secretKey) {
    this(new HalSubscriberWsClient(GsonFactory.make(), backendUrl, subscriberId, secretKey));
  }

  public SBOLAdapter subscribeToSBOL(String channel, String topic) {
    Subscription subscription = new Subscription();
    subscription.setChannel(channel);
    subscription.setTopic(topic);

    return new SBOLAdapter(this);
  }

  public SBOLAdapter subscribeToSBOL(String channel, String topic, String type) {
    Subscription subscription = new Subscription();
    subscription.setChannel(channel);
    subscription.setTopic(topic);
    subscription.setType(type);

    return new SBOLAdapter(this, subscription);
  }

  public SBOLAdapter subscribeToSBOL(String channel) {
    Subscription subscription = new Subscription();
    subscription.setChannel(channel);

    return new SBOLAdapter(this, subscription);
  }


  public ResourceSubscription subscribeToResources(String channel, String topic) {
    Subscription subscription = new Subscription();
    subscription.setChannel(channel);
    subscription.setTopic(topic);

    return new ResourceSubscription(this, subscription);
  }

  public ResourceSubscription subscribeToResources(String channel, String topic, String type) {
    Subscription subscription = new Subscription();
    subscription.setChannel(channel);
    subscription.setTopic(topic);
    subscription.setType(type);

    return new ResourceSubscription(this, subscription);
  }

  public ResourceSubscription subscribeToResources(String channel) {
    Subscription subscription = new Subscription();
    subscription.setChannel(channel);

    return new ResourceSubscription(this, subscription);
  }

  public void reset() {
    subscriber.reset();
  }

  public HalSubscriber<String> getSubscriber() {
    return subscriber;
  }

  public void setSubscriber(HalSubscriber<String> subscriber) {
    this.subscriber = subscriber;
  }
}
